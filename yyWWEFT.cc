// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Particle.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Math/MathUtils.hh"
#include <fstream> 
#include <iostream>
#include "HepMC/SimpleVector.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TLorentzVector.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/SimpleVector.h"
#include "HepMC/WeightContainer.h"

namespace Rivet {

  // yyWW production at 13 TeV
  class yyWWEFT : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(yyWWEFT);

    /// Book histograms and initialise projections before the run
    void init() {
     
      const FinalState fs(Cuts::abseta < 5.);

      ChargedFinalState cfs(Cuts::abseta < 15.0);
      declare(cfs,"CFS");

      // Project photons for dressing
      IdentifiedFinalState photon_id(fs);
      photon_id.acceptIdPair(PID::PHOTON);

      // Cuts for leptons
      Cut lepton_cuts = (Cuts::abseta < 10.0) && (Cuts::pT > 2.5*GeV);
          
      // Project dressed leptons (e/mu not from tau) with pT > 27 GeV and |eta| < 2.5
      // Both for normal and simplified phase space
      IdentifiedFinalState lep_id(fs);
      lep_id.acceptIdPair(PID::MUON);
      lep_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState lep_bare(lep_id);
      lep_bare.acceptTauDecays(false);
      DressedLeptons lep_dressed(photon_id, lep_bare, 0.1, lepton_cuts, true);
      declare(lep_dressed,"lep_dressed");

      IdentifiedFinalState n_id(fs);
      n_id.acceptIdPair(12);
      n_id.acceptIdPair(14);          
      //addProjection(n_id, "neutrinos");                                                                                                                            
      declareProjection(n_id, "neutrinos");    

      // Project jets
      //Define hadrons as everything but dressed leptons (for jet clustering)
      VetoedFinalState hadrons(fs);
      hadrons.addVetoOnThisFinalState(lep_dressed);
      //addProjection(hadrons, "hadrons");
      declareProjection(hadrons, "hadrons");
      declare(FastJets(hadrons, FastJets::ANTIKT, 0.4), "Jets");
      
      //Define tracks as everything charged but dressed leptons (for jet clustering)
      VetoedFinalState trks(cfs);
      trks.addVetoOnThisFinalState(lep_dressed);
      //addProjection(trks, "trks");
      declareProjection(trks, "trks");
      declare(FastJets(trks, FastJets::ANTIKT, 0.4), "trackJets");

      // Missing momentum
      declare(MissingMomentum(), "MET");

      // Get MET from generic invisibles
      //VetoedFinalState ivfs(fs);
      //ivfs.addVetoOnThisFinalState(VisibleFinalState(fs));
      //addProjection(ivfs, "invisible");

      //book histograms
      
      book(_h["proton_px"], "proton_px", 100, -10.0, 50.0);
      book(_h["proton_py"], "proton_py", 100, -10.0, 50.0);
      book(_h["proton_pz"], "proton_pz", 100, -6500.0, 6500.0);
      book(_h["proton_e"], "proton_e", 100, -6500.0, 6500.0);
      book(_h["proton_xi_A"], "proton_xi_A", 100, 0.0, 1.0);
      book(_h["proton_xi_C"], "proton_xi_C", 100, 0.0, 1.0);

      book(_h["leadingjet_pt"], "leadingjet_pt", 30, 10.0, 200.0);
      book(_h["leadingjet_eta"], "leadingjet_eta", 30, 10.0, 200.0);
      book(_h["leadingjet_phi"], "leadingjet_phi", 30, 10.0, 200.0);
      book(_h["leadingjet_E"], "leadingjet_E", 30, 10.0, 200.0);    

      book(_h["subleadingjet_pt"], "subleadingjet_pt", 30, 10.0, 200.0);
      book(_h["subleadingjet_eta"], "subleadingjet_eta", 30, 10.0, 200.0);
      book(_h["subleadingjet_phi"], "subleadingjet_phi", 30, 10.0, 200.0);
      book(_h["subleadingjet_E"], "subleadingjet_E", 30, 10.0, 200.0);   

      book(_h["dijet_pt"],  "dijet_pt", 30, 10.0, 200.0);
      book(_h["dijet_eta"], "dijet_eta", 30, 10.0, 200.0);
      book(_h["dijet_phi"], "dijet_phi", 30, 10.0, 200.0);
      book(_h["dijet_E"],   "dijet_E", 30, 10.0, 200.0);  

      //mww 
      book(_h["lepton_pt"],   "lepton_pt", 30, 10.0, 200.0);
      book(_h["lepton_eta"],   "lepton_eta", 30, 10.0, 200.0);
      book(_h["lepton_phi"],   "lepton_phi", 30, 10.0, 200.0);
      book(_h["lepton_E"],   "lepton_E", 30, 10.0, 200.0);

      book(_h["neutrino_pt"],   "neutrino_pt", 30, 10.0, 200.0);
      book(_h["neutrino_eta"],  "neutrino_eta", 30, 10.0, 200.0);
      book(_h["neutrino_phi"],  "neutrino_phi", 30, 10.0, 200.0);
      book(_h["neutrino_E"],    "neutrino_E", 30, 10.0, 200.0);                                      


      events=0;
      printout=0;
      eventweightcut=0;
      eventscut=0;
      eventweight=0;
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      HepMC::WeightContainer weightcont = (event.genEvent())->weights();

      // Get jets
      const Jets& jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 27*GeV && Cuts::abseta < 4.5 );
      const Jets& trkjets = apply<FastJets>(event, "trackJets").jetsByPt(Cuts::pT > 30*GeV && Cuts::abseta < 2.7 );

      // MET
      const MissingMomentum& metnew = apply<MissingMomentum>(event, "MET");
      //if (met.vectorEt().mod() < 40*GeV) vetoEvent;

      met_et=metnew.vectorEt().mod();
      met_phi=metnew.vectorEt().phi();

      const ChargedFinalState& cfs = (apply<ChargedFinalState>(event, "CFS"));
      const FinalState& neutrinos = (apply<FinalState>(event, "neutrinos"));
              
      // Get invisible
      //FIXME const FinalState& invisible = apply<VetoedFinalState>(event, "invisible");
      // Find leptons and sort by pT
      const vector<DressedLepton>& leptons = sortByPt(apply<DressedLeptons>(event, "lep_dressed").dressedLeptons());
      events++;
      //const double weight = event.weight();
      EventWeight = event.weight();
      EventNumber = events;
      eventweight=eventweight+event.weight();
      
      if (events%10000==0) std::cout <<events<<std::endl;

      myy=0;
      ntrks500MeV=0;
      ntrks100MeV=0;

      /*weightvec->clear();
                                                  
      for ( HepMC::WeightContainer::iterator w = weightcont.begin(); w != weightcont.end(); ++w ) {
        weightvec->push_back( *w  );
      }*/
      
      for (const Particle& l: leptons) { 
        _h["lepton_pt"]->fill(l.momentum().pT());  
        _h["lepton_eta"]->fill(l.momentum().eta()); 
        _h["lepton_phi"]->fill(l.momentum().phi()); 
        _h["lepton_E"]->fill(l.momentum().E());                                 
      }   

      for (const Particle& l: neutrinos.particles()) {
         _h["neutrino_pt"]->fill(l.momentum().pT());  
         _h["neutrino_eta"]->fill(l.momentum().eta());  
         _h["neutrino_phi"]->fill(l.momentum().phi());  
         _h["neutrino_E"]->fill(l.momentum().E());  
      }                                                                              

      const Jet& leadingJet = jets[0];
      TLorentzVector jet0; jet0.SetPtEtaPhiE(leadingJet.perp(), leadingJet.eta(), leadingJet.phi(),leadingJet.energy());
      _h["leadingjet_pt"]->fill(leadingJet.energy());
      _h["leadingjet_eta"]->fill(leadingJet.phi());
      _h["leadingjet_phi"]->fill(leadingJet.eta());
      _h["leadingjet_E"]->fill(leadingJet.perp()); 

      TLorentzVector jet1; jet1.SetPtEtaPhiE(0.0, 0.0, 0.0, 0.0);
      if(jets.size()>1){
        const Jet& subleadingJet = jets[1];
        jet1.SetPtEtaPhiE(subleadingJet.perp(), subleadingJet.eta(), subleadingJet.phi(), subleadingJet.energy());
        _h["subleadingjet_pt"]->fill(subleadingJet.energy());
        _h["subleadingjet_eta"]->fill(subleadingJet.phi());
        _h["subleadingjet_phi"]->fill(subleadingJet.eta());
        _h["subleadingjet_E"]->fill(subleadingJet.perp());
      }
      
      TLorentzVector dijet; dijet = jet0+jet1;
      _h["dijet_pt"]->fill(dijet.Pt());
      _h["dijet_eta"]->fill(dijet.Eta());
      _h["dijet_phi"]->fill(dijet.Phi());
      _h["dijet_E"]->fill(dijet.E()); 

      TLorentzVector y1;
      TLorentzVector y2;

      //---------------------------
      //Looping over all particles
      //---------------------------

      auto particles = event.allParticles();
      int nASideProtons =0;
      int nCSideProtons =0;

      for(int i = 0; i < particles.size(); ++i) { //loop over all
        if (particles[i].pid()==2212 && particles[i].genParticle()->status()==1){ 
          if(particles[i].pz()>0){nASideProtons++;}
          if(particles[i].pz()<0){nCSideProtons++;}
          
        }
      }
  
      if(nASideProtons==1){
        for(int i = 0; i < particles.size(); ++i) {
          if(particles[i].pz()>0 && particles[i].pid()==2212 && particles[i].genParticle()->status()==1){
            _h["proton_px"]->fill(particles[i].px()); 
            _h["proton_py"]->fill(particles[i].py()); 
            _h["proton_pz"]->fill(particles[i].pz());         
            _h["proton_e"]->fill(particles[i].E());
            _h["proton_xi_A"]->fill(1.0-(particles[i].E()/6500.));
            std::cout<<"proton info: px py pz e xi: "<< particles[i].px()<<" , "<<particles[i].py()<<" , "<<particles[i].pz()<<" , "<<particles[i].E()<<" , " <<1.0-(particles[i].E()/6500.)<<std::endl;
          }
        }
      }
      if(nCSideProtons==1){
        for(int i = 0; i < particles.size(); ++i) {
          if(particles[i].pz()<0 && particles[i].pid()==2212 && particles[i].genParticle()->status()==1){
            _h["proton_px"]->fill(particles[i].px()); 
            _h["proton_py"]->fill(particles[i].py()); 
            _h["proton_pz"]->fill(particles[i].pz());         
            _h["proton_e"]->fill(particles[i].E());
            _h["proton_xi_C"]->fill(1.0-(particles[i].E()/6500.));
          }
        }
      }

      int tmpA = 999;
      int protonA_id = -1;
      if(nASideProtons>1){
        for(int i =0; i< particles.size(); i++){
          if((particles[i].pid()==2212)&& ( particles[i].genParticle()->status()==1) && (particles[i].genParticle()->barcode() < tmpA)&& (particles[i].pz()>0)){
              protonA_id = i; 
              tmpA = particles[i].genParticle()->barcode();
            }
          }  
          _h["proton_px"]->fill(particles[protonA_id].px()); 
          _h["proton_py"]->fill(particles[protonA_id].py()); 
          _h["proton_pz"]->fill(particles[protonA_id].pz());         
          _h["proton_e"]->fill(particles[protonA_id].E());
          _h["proton_xi_A"]->fill(1.0-(particles[protonA_id].E()/6500.));
      } 
      int tmpC = 999;
      int protonC_id = -1;
      if(nCSideProtons>1){
        for(int i =0; i< particles.size(); i++){
          if((particles[i].pid()==2212)&& ( particles[i].genParticle()->status()==1) && (particles[i].genParticle()->barcode() < tmpC)&& (particles[i].pz()<0)){
              protonC_id = i; 
              tmpC = particles[i].genParticle()->barcode();
            }
          }  
          _h["proton_px"]->fill(particles[protonC_id].px()); 
          _h["proton_py"]->fill(particles[protonC_id].py()); 
          _h["proton_pz"]->fill(particles[protonC_id].pz());         
          _h["proton_e"]->fill(particles[protonC_id].E());
          _h["proton_xi_C"]->fill(1.0-(particles[protonC_id].E()/6500.));
      } 
          
    }
 
    /*
    //testing HEPMC::ancestors
    GenVertex* ivtx = p->production_vertex();

    myy = (y1+y2).M();

  eventscut++;
  eventweightcut=eventweightcut+event.weight();
 
}*/

  /// Normalise histograms etc., after the run
  void finalize() {
    const double sf(crossSection()/femtobarn/sumOfWeights());
    std::cout << events << " all  " << eventweight << std::endl;
    std::cout << eventscut << " cut  " << eventweightcut << std::endl;
    const double eff_fiducial_sel = (double)eventweightcut / (double)eventweight;      
    //const double sf   = crossSection()/femtobarn/sumW();    
        
    MSG_INFO("fid eff:     " << eff_fiducial_sel);
    MSG_INFO("xSec:        " << crossSection());
    MSG_INFO("sumW:        " << sumW());
    MSG_INFO("SF:          " << sf);

		//for (const auto p : _h){ scale(p.second, sf); }
		
		//MSG_INFO("total fiducial cross section: " << _h["lep1_pt"]->integral() << "fb");
    //for (const auto p : _h){ scale(p.second, sf); }

    //scale(jet_pt, crossSection());
    //jet_pt->Scale(crossSection());
    //for (const auto p : _h){scale(_h, xsec);   }
    //MSG_INFO("total fiducial cross section: " << _h["leptons_pt"]->integral() << "fb");
  
    //m_Tree->Write();
    //m_file->Write(); 

  }


//  private:

    /// @name Histograms
    //@{
    double xsnorm;
    double eventweight; 
    int events;
    double eventweightcut;
    int eventscut;

    int printout;
    int EventNumber;
    float xsecNorm;
    float EventWeight;
    float myy;
    int ntrks100MeV;
    int ntrks500MeV ;                            
    float met_et;
    float met_phi;
 
    map<string, Histo1DPtr> _h;
    ///@}
  };

  // The hook for the plugin system
  RIVET_DECLARE_PLUGIN(yyWWEFT);

}
