// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Particle.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Math/MathUtils.hh"
#include <fstream> 
#include <iostream>
#include "HepMC/SimpleVector.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TLorentzVector.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/SimpleVector.h"
#include "HepMC/WeightContainer.h"

namespace Rivet {

  // yyWW production at 13 TeV
  class yyWWEFT_ROOT : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(yyWWEFT_ROOT);
	/// @name Analysis methods
	///@{ 


    /// Book histograms and initialise projections before the run
    void init() {
     
      const FinalState fs(Cuts::abseta < 5.);

      ChargedFinalState cfs(Cuts::abseta < 15.0);
      declare(cfs,"CFS");

      // Project photons for dressing
      IdentifiedFinalState photon_id(fs);
      photon_id.acceptIdPair(PID::PHOTON);

      // Cuts for leptons
      Cut lepton_cuts = (Cuts::abseta < 10.0) && (Cuts::pT > 2.5*GeV);
          
      // Project dressed leptons (e/mu not from tau) with pT > 27 GeV and |eta| < 2.5
      // Both for normal and simplified phase space
      IdentifiedFinalState lep_id(fs);
      lep_id.acceptIdPair(PID::MUON);
      lep_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState lep_bare(lep_id);
      lep_bare.acceptTauDecays(false);
      DressedLeptons lep_dressed(photon_id, lep_bare, 0.1, lepton_cuts, true);
      declare(lep_dressed,"lep_dressed");

      IdentifiedFinalState n_id(fs);
      n_id.acceptIdPair(12);
      n_id.acceptIdPair(14);          
      //addProjection(n_id, "neutrinos");                                                                                                                            
      declareProjection(n_id, "neutrinos");    

      // Project jets
      //Define hadrons as everything but dressed leptons (for jet clustering)
      VetoedFinalState hadrons(fs);
      hadrons.addVetoOnThisFinalState(lep_dressed);
      //addProjection(hadrons, "hadrons");
      declareProjection(hadrons, "hadrons");
      declare(FastJets(hadrons, FastJets::ANTIKT, 0.4), "Jets");
      
      //Define tracks as everything charged but dressed leptons (for jet clustering)
      VetoedFinalState trks(cfs);
      trks.addVetoOnThisFinalState(lep_dressed);
      //addProjection(trks, "trks");
      declareProjection(trks, "trks");
      declare(FastJets(trks, FastJets::ANTIKT, 0.4), "trackJets");

      // Missing momentum
      declare(MissingMomentum(), "MET");

      // Get MET from generic invisibles
      //VetoedFinalState ivfs(fs);
      //ivfs.addVetoOnThisFinalState(VisibleFinalState(fs));
      //addProjection(ivfs, "invisible");
      
      proton_px=new std::vector<float>(); 
      proton_py=new std::vector<float>(); 
      proton_pz=new std::vector<float>(); 
      proton_e=new std::vector<float>(); 
      proton_xi_A=new std::vector<float>(); 
      proton_xi_C=new std::vector<float>(); 
 
      leadingjet_pt=new std::vector<float>(); 
      leadingjet_eta=new std::vector<float>(); 
      leadingjet_phi=new std::vector<float>(); 
      leadingjet_E=new std::vector<float>(); 
 
      subleadingjet_pt=new std::vector<float>(); 
      subleadingjet_eta=new std::vector<float>(); 
      subleadingjet_phi=new std::vector<float>(); 
      subleadingjet_E =new std::vector<float>(); 
 
      dijet_pt=new std::vector<float>();
      dijet_eta=new std::vector<float>();
      dijet_phi=new std::vector<float>();
      dijet_E =new std::vector<float>();

      lepton_pt=new std::vector<float>(); 
      lepton_eta=new std::vector<float>(); 
      lepton_phi=new std::vector<float>(); 
      lepton_E=new std::vector<float>(); 

      neutrino_pt=new std::vector<float>(); 
      neutrino_eta=new std::vector<float>(); 
      neutrino_phi=new std::vector<float>(); 
      neutrino_E  =new std::vector<float>(); 
      
      m_file = new TFile("yyWW_MG_quad.root", "RECREATE");
      m_Tree = new TTree("yyWWEFT","yyWWEFT");
      m_Tree->SetDirectory(m_file);
   
 
      m_Tree->Branch( "EventNumber",&EventNumber,"EventNumber/I") ;
      m_Tree->Branch( "xsecNorm",&xsecNorm,"xsecNorm/F") ;
      m_Tree->Branch( "EventWeight",&EventWeight,"EventWeight/F");

      m_Tree->Branch( "proton_px",&proton_px );
      m_Tree->Branch( "proton_py",&proton_py );
      m_Tree->Branch( "proton_pz",&proton_pz );
      m_Tree->Branch( "proton_e",&proton_e);
      m_Tree->Branch( "proton_xi_A",&proton_xi_A );
      m_Tree->Branch( "proton_xi_C",&proton_xi_C );
 
      m_Tree->Branch( "leadingjet_pt",&leadingjet_pt );
      m_Tree->Branch( "leadingjet_eta",&leadingjet_eta);
      m_Tree->Branch( "leadingjet_phi",&leadingjet_phi);
      m_Tree->Branch( "leadingjet_E",&leadingjet_E  );
 
      m_Tree->Branch( "subleadingjet_pt",&subleadingjet_pt );
      m_Tree->Branch( "subleadingjet_eta",&subleadingjet_eta);
      m_Tree->Branch( "subleadingjet_phi",&subleadingjet_phi);
      m_Tree->Branch( "subleadingjet_E",&subleadingjet_E  );
 
      m_Tree->Branch( "dijet_pt",&dijet_pt );
      m_Tree->Branch( "dijet_eta",&dijet_eta);
      m_Tree->Branch( "dijet_phi",&dijet_phi);
      m_Tree->Branch( "dijet_E",&dijet_E  );

      m_Tree->Branch( "lepton_pt",&lepton_pt );
      m_Tree->Branch( "lepton_eta",&lepton_eta);
      m_Tree->Branch( "lepton_phi",&lepton_phi);
      m_Tree->Branch( "lepton_E",&lepton_E  );

      m_Tree->Branch( "neutrino_pt",&neutrino_pt );
      m_Tree->Branch( "neutrino_eta",&neutrino_eta);
      m_Tree->Branch( "neutrino_phi",&neutrino_phi);
      m_Tree->Branch( "neutrino_E",&neutrino_E  );

      events=0;
      printout=0;
      eventweightcut=0;
      eventscut=0;
      eventweight=0;
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      HepMC::WeightContainer weightcont = (event.genEvent())->weights();

      // Get jets
      const Jets& jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 27*GeV && Cuts::abseta < 4.5 );
      const Jets& trkjets = apply<FastJets>(event, "trackJets").jetsByPt(Cuts::pT > 30*GeV && Cuts::abseta < 2.7 );

      // MET
      const MissingMomentum& metnew = apply<MissingMomentum>(event, "MET");
      //if (met.vectorEt().mod() < 40*GeV) vetoEvent;

      met_et=metnew.vectorEt().mod();
      met_phi=metnew.vectorEt().phi();

      const ChargedFinalState& cfs = (apply<ChargedFinalState>(event, "CFS"));
      const FinalState& neutrinos = (apply<FinalState>(event, "neutrinos"));
              
      // Get invisible
      //FIXME const FinalState& invisible = apply<VetoedFinalState>(event, "invisible");
      // Find leptons and sort by pT
      const vector<DressedLepton>& leptons = sortByPt(apply<DressedLeptons>(event, "lep_dressed").dressedLeptons());
      events++;
      //const double weight = event.weight();
      EventWeight = event.weight();
      EventNumber = events;
      eventweight=eventweight+event.weight();
      
      if (events%10000==0) std::cout <<events<<std::endl;

      myy=0;
      ntrks500MeV=0;
      ntrks100MeV=0;

      /*weightvec->clear();
                                                  
      for ( HepMC::WeightContainer::iterator w = weightcont.begin(); w != weightcont.end(); ++w ) {
        weightvec->push_back( *w  );
      }*/

      for (const Particle& l: leptons) { 

        lepton_pt->push_back(l.momentum().pT());  
        lepton_eta->push_back(l.momentum().eta()); 
        lepton_phi->push_back(l.momentum().phi()); 
        lepton_E->push_back(l.momentum().E());                                 
      }   

      for (const Particle& l: neutrinos.particles()) {
         neutrino_pt->push_back(l.momentum().pT());  
         neutrino_eta->push_back(l.momentum().eta());  
         neutrino_phi->push_back(l.momentum().phi());  
         neutrino_E->push_back(l.momentum().E());  
      }                                                                              

      const Jet& leadingJet = jets[0];
      TLorentzVector jet0; jet0.SetPtEtaPhiE(leadingJet.perp(), leadingJet.eta(), leadingJet.phi(),leadingJet.energy());
      leadingjet_pt->push_back(leadingJet.energy());
      leadingjet_eta->push_back(leadingJet.phi());
      leadingjet_phi->push_back(leadingJet.eta());
      leadingjet_E->push_back(leadingJet.perp()); 

      TLorentzVector jet1; jet1.SetPtEtaPhiE(0.0, 0.0, 0.0, 0.0);
      if(jets.size()>1){
        const Jet& subleadingJet = jets[1];
        jet1.SetPtEtaPhiE(subleadingJet.perp(), subleadingJet.eta(), subleadingJet.phi(), subleadingJet.energy());
        subleadingjet_pt->push_back(subleadingJet.energy());
        subleadingjet_eta->push_back(subleadingJet.phi());
        subleadingjet_phi->push_back(subleadingJet.eta());
        subleadingjet_E->push_back(subleadingJet.perp());
      }
      
      TLorentzVector dijet; dijet = jet0+jet1;
      dijet_pt->push_back(dijet.Pt());
      dijet_eta->push_back(dijet.Eta());
      dijet_phi->push_back(dijet.Phi());
      dijet_E ->push_back(dijet.E()); 

      TLorentzVector y1;
      TLorentzVector y2;

      //---------------------------
      //Looping over all particles
      //---------------------------

      auto particles = event.allParticles();
      int nASideProtons =0;
      int nCSideProtons =0;

      for(int i = 0; i < particles.size(); ++i) { //loop over all
        if (particles[i].pid()==2212 && particles[i].genParticle()->status()==1){ 
          if(particles[i].pz()>0){nASideProtons++;}
          if(particles[i].pz()<0){nCSideProtons++;}
          
        }
      }
  
      if(nASideProtons==1){
        for(int i = 0; i < particles.size(); ++i) {
          if(particles[i].pz()>0 && particles[i].pid()==2212 && particles[i].genParticle()->status()==1){
            proton_px->push_back(particles[i].px()); 
            proton_py->push_back(particles[i].py()); 
            proton_pz->push_back(particles[i].pz());         
            proton_e ->push_back(particles[i].E());
            proton_xi_A->push_back(1.0-(particles[i].E()/6500.));
            std::cout<<"proton info: px py pz e xi: "<< particles[i].px()<<" , "<<particles[i].py()<<" , "<<particles[i].pz()<<" , "<<particles[i].E()<<" , " <<1.0-(particles[i].E()/6500.)<<std::endl;
          }
        }
      }
      if(nCSideProtons==1){
        for(int i = 0; i < particles.size(); ++i) {
          if(particles[i].pz()<0 && particles[i].pid()==2212 && particles[i].genParticle()->status()==1){
            proton_px->push_back(particles[i].px()); 
            proton_py->push_back(particles[i].py()); 
            proton_pz->push_back(particles[i].pz());         
            proton_e ->push_back(particles[i].E());
            proton_xi_C->push_back(1.0-(particles[i].E()/6500.));
          }
        }
      }

      int tmpA = 999;
      int protonA_id = -1;
      if(nASideProtons>1){
        for(int i =0; i< particles.size(); i++){
          if((particles[i].pid()==2212)&& ( particles[i].genParticle()->status()==1) && (particles[i].genParticle()->barcode() < tmpA)&& (particles[i].pz()>0)){
              protonA_id = i; 
              tmpA = particles[i].genParticle()->barcode();
            }
          }  
            proton_px->push_back(particles[protonA_id].px()); 
            proton_py->push_back(particles[protonA_id].py()); 
            proton_pz->push_back(particles[protonA_id].pz());         
            proton_e ->push_back(particles[protonA_id].E());
            proton_xi_A->push_back(1.0-(particles[protonA_id].E()/6500.));
      } 
      int tmpC = 999;
      int protonC_id = -1;
      if(nCSideProtons>1){
        for(int i =0; i< particles.size(); i++){
          if((particles[i].pid()==2212)&& ( particles[i].genParticle()->status()==1) && (particles[i].genParticle()->barcode() < tmpC)&& (particles[i].pz()<0)){
              protonC_id = i; 
              tmpC = particles[i].genParticle()->barcode();
            }
          }  
            proton_px->push_back(particles[protonC_id].px()); 
            proton_py->push_back(particles[protonC_id].py()); 
            proton_pz->push_back(particles[protonC_id].pz());         
            proton_e ->push_back(particles[protonC_id].E());
            proton_xi_C->push_back(1.0-(particles[protonC_id].E()/6500.));
      } 
          


      eventscut++;
      eventweightcut=eventweightcut+event.weight();
      m_Tree->Fill(); 
    }
 
    /*
    //testing HEPMC::ancestors
    GenVertex* ivtx = p->production_vertex();

    myy = (y1+y2).M();

 
}*/

  /// Normalise histograms etc., after the run
  void finalize() {
    std::cout<<"rivet finalise start"<<std::endl;
    const double sf(crossSection()/femtobarn/sumOfWeights());
    std::cout << events << " all  " << eventweight << std::endl;
    std::cout << eventscut << " cut  " << eventweightcut << std::endl;
    // const double eff_fiducial_sel = (double)eventweightcut / (double)eventweight;      
    //const double sf   = crossSection()/femtobarn/sumW();    
          
    // MSG_INFO("fid eff:     " << eff_fiducial_sel);
    // MSG_INFO("xSec:        " << crossSection());
    // MSG_INFO("sumW:        " << sumW());
    // MSG_INFO("SF:          " << sf);

    //  for (const auto p : _h){ scale(p.second, sf); }
    
    //MSG_INFO("total fiducial cross section: " << _h["lep1_pt"]->integral() << "fb");
    //for (const auto p : _h){ scale(p.second, sf); }

    //scale(jet_pt, crossSection());
    //jet_pt->Scale(crossSection());
    //for (const auto p : _h){scale(_h, xsec);   }
    //MSG_INFO("total fiducial cross section: " << _h["leptons_pt"]->integral() << "fb");
  
    m_Tree->Write();
    m_file->Write(); 
    std::cout<<"rivet finalise end"<<std::endl;

  }
	///@}

  private:

    	/// @name Histograms
	///@{
	
	///@}

    double xsnorm;
    double eventweight; 
    int events;
    double eventweightcut;
    int eventscut;

    int printout;
    int EventNumber;
    float xsecNorm;
    float EventWeight;
    float myy;
    int ntrks100MeV;
    int ntrks500MeV ;                            
    float met_et;
    float met_phi;

    TFile * m_file;
    TTree * m_Tree;
 
    std::vector<float> *proton_px;
    std::vector<float> *proton_py;
    std::vector<float> *proton_pz;
    std::vector<float> *proton_e;
    std::vector<float> *proton_xi_A;
    std::vector<float> *proton_xi_C;

    std::vector<float> *leadingjet_pt;
    std::vector<float> *leadingjet_eta;
    std::vector<float> *leadingjet_phi;
    std::vector<float> *leadingjet_E;

    std::vector<float> *subleadingjet_pt;
    std::vector<float> *subleadingjet_eta;
    std::vector<float> *subleadingjet_phi;
    std::vector<float> *subleadingjet_E ;

    std::vector<float> *dijet_pt;
    std::vector<float> *dijet_eta;
    std::vector<float> *dijet_phi;
    std::vector<float> *dijet_E ;
  
    std::vector<float> *lepton_pt;
    std::vector<float> *lepton_eta;
    std::vector<float> *lepton_phi;
    std::vector<float> *lepton_E;

    std::vector<float> *neutrino_pt;
    std::vector<float> *neutrino_eta;
    std::vector<float> *neutrino_phi;
    std::vector<float> *neutrino_E  ;
 
  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(yyWWEFT_ROOT);

}

