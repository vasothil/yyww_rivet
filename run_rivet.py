#
# run with 'athena run_rivet.py'
#
include("GeneratorUtils/StdAnalysisSetup.py")
theApp.EvtMax = -1
## Specify input evgen files
import glob, AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

#svcMgr.EventSelector.InputCollections = glob.glob('INPUTFILES*root*')
#svcMgr.EventSelector.InputCollections = glob.glob("../../Samples/yyWWlnujj/mc16_13TeV.830009.H7_yyWW_semilep.merge.EVNT.e8380_e7400/*")

#Official Samples
#svcMgr.EventSelector.InputCollections = glob.glob("/eos/user/k/kristin/yyWWsemilep/lowpTyyww.EVNT.root")

svcMgr.EventSelector.InputCollections = glob.glob("/eos/user/v/vasothil/private/Samples/yyWWlnujj/SignalMC/mc16_13TeV.509021.MGPy8EG_CT14qed_yyWWlvjj_EE.merge.EVNT.e8380_e7400/*")
#svcMgr.EventSelector.InputCollections = glob.glob("/eos/user/v/vasothil/private/Samples/yyWWlnujj/SignalMC/mc16_13TeV.830009.H7_yyWW_semilep.merge.EVNT.e8380_e7400/*")

#svcMgr.EventSelector.InputCollections = glob.glob("/eos/user/v/vasothil/private/Samples/yyWWlnujj/BkgMC/mc16_13TeV.364428.MGPy8EvtGen_WZjj_lvqq_EW6.merge.EVNT.e5987_e5984/*")
#Kristin's local running samples
#svcMgr.EventSelector.InputCollections = glob.glob("/eos/user/k/kristin/yyWWlvjj/H7.EVNT.root")
#svcMgr.EventSelector.InputCollections = glob.glob("/eos/user/k/kristin/yyWWlvjj/MG295.EVNT.root")

## Now set up Rivet
from Rivet_i.Rivet_iConf import Rivet_i
topAlg += Rivet_i("Rivet")
topAlg.Rivet.Analyses = ["yyWW"]
topAlg.Rivet.AnalysisPath = os.environ['PWD']
#topAlg.Rivet.IgnoreBeamCheck
## Configure ROOT file output
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
from GaudiSvc.GaudiSvcConf import THistSvc
svcMgr += THistSvc()
#svcMgr.THistSvc.Output = ["Rivet DATAFILE='user.vasothil.509021.MG_Debug_V1.root' OPT='RECREATE'"]
#svcMgr.THistSvc.Output = ["new DATAFILE='user.vasothil.509021.MGPy8EG_CT14qed.debug_V1.root' TYP='ROOT' OPT='RECREATE'"] 
