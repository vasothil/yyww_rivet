// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Particle.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ChargedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/VisibleFinalState.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include <fstream> 
#include <iostream>
#include "HepMC/SimpleVector.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TLorentzVector.h"
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"
#include "HepMC/SimpleVector.h"
#include "HepMC/WeightContainer.h"

namespace Rivet {


  /// WW production at 13 TeV
  class yyWW : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(yyWW);


    /// Book histograms and initialise projections before the run
    void init() {
     
      const FinalState fs(Cuts::abseta < 5.);

      ChargedFinalState cfs(Cuts::abseta < 15.0);
      declare(cfs,"CFS");

      // Project photons for dressing
      IdentifiedFinalState photon_id(fs);
      photon_id.acceptIdPair(PID::PHOTON);

      // Cuts for leptons
      Cut lepton_cuts = (Cuts::abseta < 10.0) && (Cuts::pT > 2.5*GeV);
          
      // Project dressed leptons (e/mu not from tau) with pT > 27 GeV and |eta| < 2.5
      // Both for normal and simplified phase space
      IdentifiedFinalState lep_id(fs);
      lep_id.acceptIdPair(PID::MUON);
      lep_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState lep_bare(lep_id);
      lep_bare.acceptTauDecays(false);
      DressedLeptons lep_dressed(photon_id, lep_bare, 0.1, lepton_cuts, true);
      declare(lep_dressed,"lep_dressed");

      IdentifiedFinalState n_id(fs);
      n_id.acceptIdPair(12);
      n_id.acceptIdPair(14);          
      addProjection(n_id, "neutrinos");                                                                                                                            


      // Project jets
      //Define hadrons as everything but dressed leptons (for jet clustering)
      VetoedFinalState hadrons(fs);
      hadrons.addVetoOnThisFinalState(lep_dressed);
      addProjection(hadrons, "hadrons");
      declare(FastJets(hadrons, FastJets::ANTIKT, 0.4), "Jets");
      //

      //Define tracks as everything charged but dressed leptons (for jet clustering)
      VetoedFinalState trks(cfs);
      trks.addVetoOnThisFinalState(lep_dressed);
      addProjection(trks, "trks");
      declare(FastJets(trks, FastJets::ANTIKT, 0.4), "trackJets");

      // Missing momentum
      declare(MissingMomentum(), "MET");

      // Get MET from generic invisibles
      //VetoedFinalState ivfs(fs);
      //ivfs.addVetoOnThisFinalState(VisibleFinalState(fs));
      //addProjection(ivfs, "invisible");

      tracks_E=new std::vector<float>();
      tracks_phi=new std::vector<float>();
      tracks_eta=new std::vector<float>();
      tracks_pt=new std::vector<float>();

      leptons_E=new std::vector<float>();
      leptons_phi=new std::vector<float>();
      leptons_eta=new std::vector<float>();
      leptons_pt=new std::vector<float>();
      leptons_charge=new std::vector<float>();

      neutrinos_E=new std::vector<float>();
      neutrinos_phi=new std::vector<float>();
      neutrinos_eta=new std::vector<float>();
      neutrinos_pt=new std::vector<float>();
      neutrinos_charge=new std::vector<float>();

      weightvec=new std::vector<float>();                                                                     
      proton_px=new std::vector<float>();  
      proton_py=new std::vector<float>();
      proton_pz=new std::vector<float>();
      proton_e=new std::vector<float>();                        

      W_Bosons_px=new std::vector<float>();
      W_Bosons_py=new std::vector<float>();
      W_Bosons_pz=new std::vector<float>();
      W_Bosons_e=new std::vector<float>();
      //W_Bosons=new std::vector<float>();

      Lep_W_Bosons_px =new std::vector<float>();
      Lep_W_Bosons_py =new std::vector<float>();
      Lep_W_Bosons_pz =new std::vector<float>();
      Lep_W_Bosons_e =new std::vector<float>();
      Lep_W_Bosons_pid =new std::vector<float>();

      Had_W_Bosons_px =new std::vector<float>();
      Had_W_Bosons_py =new std::vector<float>();
      Had_W_Bosons_pz =new std::vector<float>();
      Had_W_Bosons_e =new std::vector<float>();

      jet_pt=new std::vector<float>();
      jet_E=new std::vector<float>();
      jet_phi=new std::vector<float>();
      jet_eta=new std::vector<float>();                                                                                                                                  

      trkjet_pt=new std::vector<float>();
      trkjet_E=new std::vector<float>();
      trkjet_phi=new std::vector<float>();
      trkjet_eta=new std::vector<float>();

      m_file = new TFile("yyWW_MG_quad.root", "RECREATE");
//      m_file = new TFile("yyWW_MG.root", "RECREATE");
//      m_file = new TFile("yyWW_bkgMG.root", "RECREATE");
      m_Tree = new TTree("yyWWtruth","yyWWtruth");
      m_Tree->SetDirectory(m_file);
   
      m_Tree->Branch( "leptons_pt",  &leptons_pt );
      m_Tree->Branch( "leptons_eta",  &leptons_eta );
      m_Tree->Branch( "leptons_phi",  &leptons_phi );
      m_Tree->Branch( "leptons_E",  &leptons_E );
      m_Tree->Branch( "leptons_charge",  &leptons_charge );

      m_Tree->Branch( "neutrinos_pt",  &neutrinos_pt );
      m_Tree->Branch( "neutrinos_eta",  &neutrinos_eta );
      m_Tree->Branch( "neutrinos_phi",  &neutrinos_phi );
      m_Tree->Branch( "neutrinos_E",  &neutrinos_E );
      m_Tree->Branch( "neutrinos_charge",  &neutrinos_charge );

      m_Tree->Branch( "ntrks100MeV",  &ntrks100MeV, ntrks100MeV );
      m_Tree->Branch( "ntrks500MeV",  &ntrks500MeV, ntrks500MeV );                                                                                                                
      m_Tree->Branch( "tracks_eta",  &tracks_eta );
      m_Tree->Branch( "tracks_pt",  &tracks_pt );
      m_Tree->Branch( "tracks_E",  &tracks_E );
      m_Tree->Branch( "tracks_phi",  &tracks_phi );

      m_Tree->Branch( "weightvec",  &weightvec );

      m_Tree->Branch( "proton_px",  &proton_px );
      m_Tree->Branch( "proton_py",  &proton_py );
      m_Tree->Branch( "proton_pz",  &proton_pz );
      m_Tree->Branch( "proton_e",  &proton_e );

      m_Tree->Branch( "W_Bosons_px",  &W_Bosons_px );
      m_Tree->Branch( "W_Bosons_py",  &W_Bosons_py );
      m_Tree->Branch( "W_Bosons_pz",  &W_Bosons_pz );
      m_Tree->Branch( "W_Bosons_e",  &W_Bosons_e );

      m_Tree->Branch( "Lep_W_Bosons_px", &Lep_W_Bosons_px );
      m_Tree->Branch( "Lep_W_Bosons_py", &Lep_W_Bosons_py );
      m_Tree->Branch( "Lep_W_Bosons_pz", &Lep_W_Bosons_pz );
      m_Tree->Branch( "Lep_W_Bosons_e",  &Lep_W_Bosons_e );
      m_Tree->Branch( "Lep_W_Bosons_pid",  &Lep_W_Bosons_pid );

      m_Tree->Branch( "Had_W_Bosons_px", &Had_W_Bosons_px );
      m_Tree->Branch( "Had_W_Bosons_py", &Had_W_Bosons_py );
      m_Tree->Branch( "Had_W_Bosons_pz", &Had_W_Bosons_pz );
      m_Tree->Branch( "Had_W_Bosons_e",  &Had_W_Bosons_e );

      m_Tree->Branch( "jet_pt",  &jet_pt );
      m_Tree->Branch( "jet_eta",  &jet_eta );
      m_Tree->Branch( "jet_phi",  &jet_phi );
      m_Tree->Branch( "jet_E",  &jet_E );

      m_Tree->Branch( "trkjet_pt",  &trkjet_pt );
      m_Tree->Branch( "trkjet_eta",  &trkjet_eta );
      m_Tree->Branch( "trkjet_phi",  &trkjet_phi );
      m_Tree->Branch( "trkjet_E",  &trkjet_E );

      m_Tree->Branch( "met_et",&met_et,"met_et/F") ;
      m_Tree->Branch( "met_phi",&met_phi,"met_phi/F") ;
                                                                       
      events=0;
      m_Tree->Branch( "EventNumber",&EventNumber,"EventNumber/I") ;
      m_Tree->Branch( "xsecNorm",&xsecNorm,"xsecNorm/F") ;
      m_Tree->Branch( "EventWeight",&EventWeight,"EventWeight/F");

      printout=0;
      eventweightcut=0;
      eventscut=0;
      eventweight=0;
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

   HepMC::WeightContainer weightcont = (event.genEvent())->weights();

   //for ( HepMC::WeightContainer::iterator w = weightcont.begin(); w != weightcont.end(); ++w ) {
 //  weightvec->push_back( *w  ); 
 //  }

      // Get jets
      const Jets& jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 30*GeV && Cuts::abseta < 4.5 );
      const Jets& trkjets = apply<FastJets>(event, "trackJets").jetsByPt(Cuts::pT > 30*GeV && Cuts::abseta < 2.7 );

      //
      const MissingMomentum& metnew = apply<MissingMomentum>(event, "MET");
//      if (met.vectorEt().mod() < 40*GeV) vetoEvent;

   met_et=metnew.vectorEt().mod();
   met_phi=metnew.vectorEt().phi();

   const ChargedFinalState& cfs = (apply<ChargedFinalState>(event, "CFS"));
   const FinalState& neutrinos = (apply<FinalState>(event, "neutrinos"));

              
      // Get invisible
      //FIXME const FinalState& invisible = apply<VetoedFinalState>(event, "invisible");
      // Find leptons and sort by pT
      const vector<DressedLepton>& leptons = sortByPt(apply<DressedLeptons>(event, "lep_dressed").dressedLeptons());
      events++;
      //const double weight = event.weight();
      EventWeight = event.weight();
      EventNumber = events;
      eventweight=eventweight+event.weight();
      
     if (events%10000==0) std::cout <<events<<std::endl;

     myy=0;
     ntrks500MeV=0;
     ntrks100MeV=0;
  
     tracks_E->clear();	
     tracks_phi->clear();                                                         
     tracks_eta->clear();
     tracks_pt->clear(); 
     weightvec->clear();

     proton_e->clear();
     proton_px->clear();
     proton_py->clear();
     proton_pz->clear();

     W_Bosons_e->clear();
     W_Bosons_px->clear();
     W_Bosons_py->clear();
     W_Bosons_pz->clear();

     Lep_W_Bosons_px->clear(); 
     Lep_W_Bosons_py->clear(); 
     Lep_W_Bosons_pz->clear(); 
     Lep_W_Bosons_e->clear();  
     Lep_W_Bosons_pid->clear();

     Had_W_Bosons_px->clear(); 
     Had_W_Bosons_py->clear(); 
     Had_W_Bosons_pz->clear(); 
     Had_W_Bosons_e->clear();  

     jet_E->clear();
     jet_phi->clear();
     jet_eta->clear();
     jet_pt->clear();

     trkjet_E->clear();
     trkjet_phi->clear();
     trkjet_eta->clear();
     trkjet_pt->clear();

     leptons_E->clear();
     leptons_phi->clear();
     leptons_eta->clear();
     leptons_pt->clear();
     leptons_charge->clear(); 
                                                             
     neutrinos_E->clear();
     neutrinos_phi->clear();
     neutrinos_eta->clear();
     neutrinos_pt->clear();
     neutrinos_charge->clear();
                                                                                  
    for ( HepMC::WeightContainer::iterator w = weightcont.begin(); w != weightcont.end(); ++w ) {
    weightvec->push_back( *w  );
    }

     // std::cout<<"************************   "<< events << std::endl;
     foreach (const Particle& p, sortByPt(cfs.particles())) { 

      if (abs(p.pid())==13) continue;
      if (abs(p.pid())==11) continue;

      if (p.momentum().pT()>0.5*GeV && fabs(p.momentum().eta())<2.5) ntrks500MeV++;
      if (p.momentum().pT()>0.1*GeV && fabs(p.momentum().eta())<2.5) ntrks100MeV++;                                            

     if (tracks_eta->size()<100) {
	      tracks_eta->push_back(p.momentum().eta());
	      tracks_pt->push_back(p.momentum().pT() ); 
	      tracks_E->push_back(p.momentum().E());
        tracks_phi->push_back(p.momentum().phi() ); 
      }}

     for (const Particle& l: leptons) { 
       leptons_E->push_back(l.momentum().E());
       leptons_phi->push_back(l.momentum().phi());
       leptons_eta->push_back(l.momentum().eta());
       leptons_pt->push_back(l.momentum().pT() );
       leptons_charge->push_back(l.pid() );                                     
     }           

     for (const Particle& l: neutrinos.particles()) {
       neutrinos_E->push_back(l.momentum().E());
       neutrinos_phi->push_back(l.momentum().phi()); 
       neutrinos_eta->push_back(l.momentum().eta());
       neutrinos_pt->push_back(l.momentum().pT() ); 
       neutrinos_charge->push_back(l.pid() ); 
     }                                                                                

    for (const Jet& j: jets) {
    jet_E->push_back(j.energy());
    jet_phi->push_back(j.phi());
    jet_eta->push_back(j.eta());
    jet_pt->push_back(j.perp());
    }


    for (const Jet& j: trkjets) {
    trkjet_E->push_back(j.energy());
    trkjet_phi->push_back(j.phi());
    trkjet_eta->push_back(j.eta());
    trkjet_pt->push_back(j.perp());
    }

TLorentzVector y1;
TLorentzVector y2;


//--------------------W boson determination

bool WBoson_Ancestor = false;

std::vector<const GenParticle*> Potential_WPos;
std::vector<const GenParticle*> Potential_WNeg;
std::vector<const GenParticle*> Potential_Leptons;
std::vector<const GenParticle*> Potential_Quarks;

//std::cout<< " pdg_id  barcode status "<<std::endl;
 foreach (const GenParticle* p, particles(event.genEvent())) {

 //testing HEPMC::ancestors
 GenVertex* ivtx = p->production_vertex();
 if(ivtx !=0){
  auto pdg_id = p->pdg_id();
//  bool WBoson_Ancestor = false;
  foreach (const GenParticle* ancestor, particles(ivtx, HepMC::ancestors)) {
   if (abs(ancestor->pdg_id()) == 24){WBoson_Ancestor=true;}
 // }
 }
}

// if( (abs(p->pdg_id()) == 24) || ( (0 < abs(p->pdg_id()) ) && ( 15 > abs(p->pdg_id()) ) )){
  //    std::cout<<p->pdg_id()<<"  "<<p->barcode()<<"  "<<p->status()<<std::endl;
// }
//Step 1: look for WW pair.
if(p->pdg_id() == 24){Potential_WPos.push_back(p);}
if(p->pdg_id() == -24){Potential_WNeg.push_back(p);}

//Step 2: look for leptons with final status 1
if((p->status() ==1) && (abs(p->pdg_id())>10) && (abs(p->pdg_id())<15)){Potential_Leptons.push_back(p);}

//Step 3: quark pairs
if((abs(p->pdg_id())>0) && (abs(p->pdg_id())<7)){Potential_Quarks.push_back(p);}

//------------
   y1.SetPxPyPzE(p->momentum().px(),p->momentum().py(),p->momentum().pz(),p->momentum().e() );

if (p->pdg_id()==22 && p->barcode()==3) {
     y1.SetPxPyPzE(p->momentum().px(),p->momentum().py(),p->momentum().pz(),p->momentum().e() );
} 
if (p->pdg_id()==22 && p->barcode()==4) {
     y2.SetPxPyPzE(p->momentum().px(),p->momentum().py(),p->momentum().pz(),p->momentum().e() );
}

std::cout<<"PARTICLE barcode status id: "<<p->barcode()<< " , "<<p->status()<<" , "<< p->pdg_id()<<std::endl;
if (p->pdg_id()==2212 && p->status()==4) {
  //std::cout<<"PARTICLE barcode : "<<p->barcode()<<std::endl;
if (isFromIncomingProton(p)) {
     //std::cout<<"evt is from incoming proton"<<std::endl;
     proton_py->push_back(p->momentum().py());
     proton_px->push_back(p->momentum().px() );
     proton_e->push_back(p->momentum().e()); 
     proton_pz->push_back(p->momentum().pz() );                                                                                                                           
   }}                                                                                                                                                             
} //end of foreach loop




//----------------------------------------------
//Determining if our W bosons are worth saving:
//----------------------------------------------
//std::cout<<"----------------------- Determination -----------------------"<<std::endl;
bool Wbosons_found = false;
bool leptonpair_found = false;
bool qqpair_found = false;

double leptonicW_charge = 0;
double hadronicW_charge = 0;

int largestBarcode_combination = 0;
int WPos_Final_ID = -99; //Potential_WPos
int WNeg_Final_ID = -99; //Potential_WNeg

std::vector<const GenParticle*> qq_Pair_Barcode_Final;

//-------------------------------------
//looping over all W bosons from event:
//-------------------------------------
for(int i = 0; i<Potential_WPos.size(); i++){
	for(int j = 0; j<Potential_WNeg.size(); j++){  
     		int barcodeWW = Potential_WPos[i]->barcode() + Potential_WNeg[j]->barcode();
     		int diff = abs(Potential_WPos[i]->barcode() - Potential_WNeg[j]->barcode());
     		if(diff ==1 && (barcodeWW > largestBarcode_combination)){
        		WPos_Final_ID = i;
        		WNeg_Final_ID = j;        
     		}
  	}
}
//----------------------------------------------------------
//Decide if we save the W boson information from this event
//----------------------------------------------------------

if((WPos_Final_ID != -99) && (WNeg_Final_ID != -99)){ 
       //std::cout<<"Final W+: "<< Potential_WPos[WPos_Final_ID]->barcode()<<std::endl;
      //std::cout<<"Final W-: "<< Potential_WNeg[WNeg_Final_ID]->barcode()<<std::endl;
       Wbosons_found = true;
}

//----------------------------------------
//W bosons found, 
//Check if the event contains semi lep FS
//----------------------------------------

if(Wbosons_found){

        //---------------------------------------------
        // find quarks and determine hadronic W charge
        // --------------------------------------------

	int nqqPairs =0;
	float qqPair_charge = -99;
	double q1_charge= -99;
	double q2_charge= -99;
	double mycharge= -99;

	for(int i =0; i<Potential_Quarks.size();i++){
   		int ivtx1_barcode = Potential_Quarks[i]->production_vertex()->barcode();
   		int q1_pdgid = Potential_Quarks[i]->pdg_id();
   		//std::cout<<"quark, pdgid, ivtx: "<<Potential_Quarks[i]->pdg_id()<<" "<<Potential_Quarks[i]->production_vertex()->barcode()<<std::endl;
   		for(int j =i+1; j<Potential_Quarks.size();j++){
       			int ivtx2_barcode = Potential_Quarks[j]->production_vertex()->barcode();
       			int q2_pdgid = Potential_Quarks[j]->pdg_id();
       			if((ivtx1_barcode == ivtx2_barcode) && (abs(q1_pdgid) != abs(q2_pdgid))){
           			nqqPairs++;
           			std::vector<std::vector<int>> quark_info = {{0,0},{0,0}};
           			quark_info[0][0] = (q1_pdgid);
           			quark_info[1][0] = (q2_pdgid);
           			quark_info[0][1] = q1_pdgid % 2;
           			quark_info[1][1] = q2_pdgid % 2;
		           	for(int i = 0; i<2; i++){ 
                			if((quark_info[i][1] ==  1) && (quark_info[i][0] >0)){ mycharge = -1./3;}
                			if((quark_info[i][1] == -1) && (quark_info[i][0] <0)){ mycharge = +1./3;}
                			if((quark_info[i][1] ==  0) && (quark_info[i][0] >0)){ mycharge = +2./3;}
                			if((quark_info[i][1] ==  0) && (quark_info[i][0] <0)){ mycharge = -2./3;}
                			if( i == 0){q1_charge = mycharge;}
                			if( i == 1){q2_charge = mycharge;}
           			}
           			qqPair_charge = q1_charge + q2_charge;
        		}
   		}
	}	
	if(nqqPairs == 1){hadronicW_charge = qqPair_charge; leptonicW_charge= -1*qqPair_charge;
	//std::cout<<"qqbar pair found. W boson charge: "<<hadronicW_charge<<std::endl;
        qqpair_found = true;
	}
}
if(Wbosons_found && qqpair_found){

        //---------------------------------------------
        // find leptons matching to leptonic W
        // --------------------------------------------
	//for(int i =0; i<Potential_Leptons.size();i++){
      	//	std::cout<<"leptons saved: "<< Potential_Leptons[i]->pdg_id()<<" "<<Potential_Leptons[i]->barcode()<<" "<<Potential_Leptons[i]->status()<<std::endl;	}
 	//----------leptons

	int total_charge = -99;
	int largest_lep_combineBarcode = 0;
	int lepton1_final_ID = -99;
	int lepton2_final_ID = -99;
	int nleptonPairs = 0;

	if(Potential_Leptons.size()<3){
   		int pdgid_lep_1 = Potential_Leptons[0]->pdg_id();
   		int pdgid_lep_2 = Potential_Leptons[1]->pdg_id();
   		if(((abs(pdgid_lep_1) ==11 && abs(pdgid_lep_2) == 12) || (abs(pdgid_lep_1) ==12 && abs(pdgid_lep_2) == 11) )&& (pdgid_lep_1*pdgid_lep_2 < 0)){
 			//std::cout<<"nlep = 2 electronic pair found: "<<std::endl; 
       			lepton1_final_ID = 0; 
       			lepton2_final_ID = 1; 
       			nleptonPairs++;
                        if(pdgid_lep_1 == 11 || pdgid_lep_2 == 11){total_charge = -1;}
                        if(pdgid_lep_1 ==-11 || pdgid_lep_2 ==-11){total_charge = +1;}
 
  		}		
   		if(((abs(pdgid_lep_1) ==13 && abs(pdgid_lep_2) == 14) ||( abs(pdgid_lep_1) ==14 && abs(pdgid_lep_2) == 13) )&& (pdgid_lep_1*pdgid_lep_2 < 0)){
 			//std::cout<<"nlep = 2 muonic pair found: "<<std::endl; 
       			lepton1_final_ID = 0; 
       			lepton2_final_ID = 1; 
       			nleptonPairs++;
                        if(pdgid_lep_1 == 13 || pdgid_lep_2 == 13){total_charge = -1;}
                        if(pdgid_lep_1 ==-13 || pdgid_lep_2 ==-13){total_charge = +1;}
   		}
                //std::cout<<"one lepton pair found, check total charge == leptonic w charge: "<<total_charge <<" = "<<leptonicW_charge<<std::endl;
                if(total_charge == leptonicW_charge){leptonpair_found = true;}
	}	

	else{
                int sum_barcodes = 999999999;
                //total_charge;
  		for(int i =0; i<Potential_Leptons.size();i++){
     			for(int j =i+1; j<Potential_Leptons.size();j++){
		                int pdgid_lep_1 = Potential_Leptons[i]->pdg_id();
                		int pdgid_lep_2 = Potential_Leptons[j]->pdg_id();
        			//e nu_e pair
        			if((abs(Potential_Leptons[i]->pdg_id()) == 11) && (abs(Potential_Leptons[j]->pdg_id()) ==12) && (Potential_Leptons[i]->pdg_id()*Potential_Leptons[j]->pdg_id() < 0) ) {                    
	                	        if(pdgid_lep_1 == 11 || pdgid_lep_2 == 11){total_charge = -1;}
        		                if(pdgid_lep_1 ==-11 || pdgid_lep_2 ==-11){total_charge = +1;}

                                        if(total_charge == leptonicW_charge){
 						int lep_sum_barcodes = Potential_Leptons[i]->barcode() + Potential_Leptons[j]->barcode();
					    	if(lep_sum_barcodes < sum_barcodes){
                                			sum_barcodes = lep_sum_barcodes; 
				           		lepton1_final_ID = i;
                                            		lepton2_final_ID = j;}
                                            		nleptonPairs++;
                                        }

  					//std::cout<<"barcodes found: "<< Potential_Leptons[i]->barcode() <<" "<< Potential_Leptons[j]->barcode()<<std::endl ;
  					//std::cout<<"nlep >2 electronic pair found: "<<std::endl; 
                     			//std::cout<<"leptons id 1 2: "<<Potential_Leptons[i]->pdg_id()<<" "<<Potential_Leptons[j]->pdg_id()<<std::endl;
 					//int lep_sum_barcodes = Potential_Leptons[i]->barcode() + Potential_Leptons[j]->barcode();
                                        //std::cout<<"check lep_sum_barcodes: "<<lep_sum_barcodes<<std::endl;
					//if(lep_sum_barcodes < sum_barcodes){lep_sum_barcodes == sum_barcodes;
             				//lepton1_final_ID = i; 
             				//lepton2_final_ID = j;}
             				//nleptonPairs++; 
        			}
        			//mu nu_mu pair
        			if((abs(Potential_Leptons[i]->pdg_id()) == 13) && (abs(Potential_Leptons[j]->pdg_id()) ==14)&& (Potential_Leptons[i]->pdg_id()*Potential_Leptons[j]->pdg_id() < 0) ){
                                        if(pdgid_lep_1 == 13 || pdgid_lep_2 == 13){total_charge = -1;}
                                        if(pdgid_lep_1 ==-13 || pdgid_lep_2 ==-13){total_charge = +1;}
                                        if(total_charge == leptonicW_charge){
                                            	int lep_sum_barcodes = Potential_Leptons[i]->barcode() + Potential_Leptons[j]->barcode();
                                            	if(lep_sum_barcodes < sum_barcodes){
                                            		sum_barcodes = lep_sum_barcodes;
							                                  lepton1_final_ID = i;
                                            		lepton2_final_ID = j;}
                                            		nleptonPairs++;
                                        }

   					//std::cout<<"barcodes found: "<< Potential_Leptons[i]->barcode() <<" "<< Potential_Leptons[j]->barcode()<<std::endl ;
   					//std::cout<<"nlep > 2 muonic pair found: "<<std::endl; 
                                        //std::cout<<"leptons id 1 2: "<<Potential_Leptons[i]->pdg_id()<<" "<<Potential_Leptons[j]->pdg_id()<<std::endl;
                                //        int lep_sum_barcodes = Potential_Leptons[i]->barcode() + Potential_Leptons[j]->barcode();
				//	std::cout<<"check lep_sum_barcodes: "<<lep_sum_barcodes<<std::endl;
                                //        if(lep_sum_barcodes < sum_barcodes){lep_sum_barcodes == sum_barcodes;
           		        //	lepton1_final_ID = i; 
           			//	lepton2_final_ID = j;}
           			//	nleptonPairs++;
 				}
     			}
  		}
	}			
//std::cout<<"check lepton pair No. : "<<nleptonPairs<<std::endl;
//final lepton chosen:
if(nleptonPairs==1){
    int lepton1_pdgid = Potential_Leptons[lepton1_final_ID]->pdg_id();
    int lepton2_pdgid = Potential_Leptons[lepton2_final_ID]->pdg_id();

    if(lepton1_pdgid == 11 ||lepton1_pdgid == 13 || lepton2_pdgid == 11 ||lepton2_pdgid == 13 ){leptonicW_charge = -1; }
    if(lepton1_pdgid == -11 ||lepton1_pdgid == -13 || lepton2_pdgid == -11 ||lepton2_pdgid == -13 ){leptonicW_charge = +1;}

    //std::cout<<"lepton pair, pdgid barcode status"<<std::endl;
    //std::cout<< Potential_Leptons[lepton1_final_ID]->pdg_id()<<" "<<Potential_Leptons[lepton1_final_ID]->barcode()<<" "<<Potential_Leptons[lepton1_final_ID]->status()<<std::endl;
    //std::cout<< Potential_Leptons[lepton2_final_ID]->pdg_id()<<" "<<Potential_Leptons[lepton2_final_ID]->barcode()<<" "<<Potential_Leptons[lepton2_final_ID]->status()<<std::endl;
    leptonpair_found = true;
}
if(nleptonPairs>1){//need to start considering quarks and then coming back to this}
   // std::cout<<"Lepton Pair greater than 1: "<< nleptonPairs<<std::endl;

    int lepton1_pdgid = Potential_Leptons[lepton1_final_ID]->pdg_id();
    int lepton2_pdgid = Potential_Leptons[lepton2_final_ID]->pdg_id();

    if(lepton1_pdgid == 11 ||lepton1_pdgid == 13 || lepton2_pdgid == 11 ||lepton2_pdgid == 13 ){leptonicW_charge = -1; }
    if(lepton1_pdgid == -11 ||lepton1_pdgid == -13 || lepton2_pdgid == -11 ||lepton2_pdgid == -13 ){leptonicW_charge = +1;}


    //std::cout<<"lepton pair, pdgid barcode status"<<std::endl;
    //std::cout<< Potential_Leptons[lepton1_final_ID]->pdg_id()<<" "<<Potential_Leptons[lepton1_final_ID]->barcode()<<" "<<Potential_Leptons[lepton1_final_ID]->status()<<std::endl;
    //std::cout<< Potential_Leptons[lepton2_final_ID]->pdg_id()<<" "<<Potential_Leptons[lepton2_final_ID]->barcode()<<" "<<Potential_Leptons[lepton2_final_ID]->status()<<std::endl;

    leptonpair_found = true;

}//more than one lepton pair found

}//end of W bososns and qq pair found if statement



//final requirements: W pos and W neg found, lepton pair found, quark pair found

//std::cout<<"------------ FINAL CHECKS ------------"<<std::endl;
if(leptonpair_found && Wbosons_found && qqpair_found){
	//std::cout<<"check hadronic and leptonic W charges = "<<leptonicW_charge + hadronicW_charge<<std::endl;

	//Potential_WPos[WPos_Final_ID]
	//Potential_WNeg[WNeg_Final_ID]
	W_Bosons_py->push_back(Potential_WPos[WPos_Final_ID]->momentum().py());
	W_Bosons_px->push_back(Potential_WPos[WPos_Final_ID]->momentum().px());
	W_Bosons_e->push_back(Potential_WPos[WPos_Final_ID]->momentum().e());
	W_Bosons_pz->push_back(Potential_WPos[WPos_Final_ID]->momentum().pz());

        W_Bosons_py->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().py());
        W_Bosons_px->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().px());
        W_Bosons_e->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().e());
        W_Bosons_pz->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().pz());

if(hadronicW_charge == +1){
        Had_W_Bosons_py->push_back(Potential_WPos[WPos_Final_ID]->momentum().py());
        Had_W_Bosons_px->push_back(Potential_WPos[WPos_Final_ID]->momentum().px());
        Had_W_Bosons_e->push_back(Potential_WPos[WPos_Final_ID]->momentum().e());
        Had_W_Bosons_pz->push_back(Potential_WPos[WPos_Final_ID]->momentum().pz());
}

if(leptonicW_charge == +1){
        Lep_W_Bosons_py->push_back(Potential_WPos[WPos_Final_ID]->momentum().py());
        Lep_W_Bosons_px->push_back(Potential_WPos[WPos_Final_ID]->momentum().px());
        Lep_W_Bosons_e->push_back(Potential_WPos[WPos_Final_ID]->momentum().e());
        Lep_W_Bosons_pz->push_back(Potential_WPos[WPos_Final_ID]->momentum().pz());
        Lep_W_Bosons_pid->push_back(Potential_WPos[WPos_Final_ID]->pdg_id());
}
if(hadronicW_charge == -1){
        Had_W_Bosons_py->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().py());
        Had_W_Bosons_px->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().px());
        Had_W_Bosons_e->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().e());
        Had_W_Bosons_pz->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().pz());
}

if(leptonicW_charge == -1){
        Lep_W_Bosons_py->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().py());
        Lep_W_Bosons_px->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().px());
        Lep_W_Bosons_e->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().e());
        Lep_W_Bosons_pz->push_back(Potential_WNeg[WNeg_Final_ID]->momentum().pz());
        Lep_W_Bosons_pid->push_back(Potential_WNeg[WNeg_Final_ID]->pdg_id());
}


}

//
//if(Wbosons_found && leptonpair_found && qqpair_found){save w boson info}
//---- end of determination



   myy = (y1+y2).M();
   m_Tree->Fill();

   eventscut++;
   eventweightcut=eventweightcut+event.weight();
 
}

    /// Normalise histograms etc., after the run
    void finalize() {
      const double sf(crossSection()/femtobarn/sumOfWeights());

    std::cout << events << " all  " << eventweight << std::endl;
    std::cout << eventscut << " cut  " << eventweightcut << std::endl;

      m_Tree->Write();
      m_file->Write();  
    }


  private:

    // Check whether particle comes from proton
    bool isFromIncomingProton(const Particle& p) {
      //std::cout <<"-----------IsFromIncomingProton-----------" << std::endl;
      bool isfromBeam = false;
      if (p.genParticle() == NULL) return false;
      const GenParticle* part = p.genParticle();
      GenVertex* ivtx = part->production_vertex();     
      int nvert=0;
      if (part->barcode()==10005) return true;
      if (part->barcode()>10005 && !(part->barcode()==10007)) return false;
      //std::cout<<"ivtx particles in size" <<ivtx->particles_in_size()<<std::endl;
      //while production vertex
      while(ivtx){
        nvert++;
        if (ivtx->particles_in_size() == 2 && ivtx->particles_out_size()==4){
          for ( GenVertex::particles_in_const_iterator w = ivtx->particles_in_const_begin(); w != ivtx->particles_in_const_end(); ++w ) {
            std::cout<<"pdgid check IN : "<< (*w)->pdg_id()<<std::endl;
          }
          for ( GenVertex::particles_out_const_iterator w = ivtx->particles_out_const_begin(); w != ivtx->particles_out_const_end(); ++w ) {
            std::cout<<"pdgid check OUT: "<< (*w)->pdg_id()<<std::endl;
          }         
        }
          
	      if (ivtx->particles_in_size() <= 1 ){ 
          isfromBeam = (part->pdg_id() == 2212)&&(  part->barcode() == 1 || part->barcode() == 2 || part->barcode() == 10004 || part->barcode() == 10007 || 
          part->barcode() == 10005 || part->barcode() == 10006 || part->barcode() == 12 || part->barcode() ==11 || part->barcode() <21 ); 
          break; 
        }
	      if (isfromBeam == true) break;
	      //ivtx = part->production_vertex();
	      if ( !(ivtx) ) {
	        isfromBeam = (part->pdg_id() == 2212)&&(  part->barcode() == 1 || part->barcode() == 2 || part->barcode() == 10004 || part->barcode() == 10007 || part->barcode() == 10005 || part->barcode() == 10006) ;
	        break;
        } // reached beam
	      return (isfromBeam);
	    }

      if ( !(ivtx) ) {
	      isfromBeam = (part->pdg_id() == 2212)&&(  part->barcode() == 1 || part->barcode() == 2 || part->barcode() == 10004 || part->barcode() == 10007 || part->barcode() == 10005 || part->barcode() == 10006) ;
      }
      //std::cout <<"End of function: from beam: " << isfromBeam<< std::endl;
      return isfromBeam;
    }

     

    /// @name Histograms
    //@{
double xsnorm;
double eventweight; 
   int events;
double eventweightcut;
   int eventscut;


    TFile * m_file;
    TTree * m_Tree;

    int printout;

    int EventNumber;
    float xsecNorm;
    float EventWeight;
    float myy;
    int ntrks100MeV;
    int ntrks500MeV ;
                       
    std::vector<float> *tracks_pt;
    std::vector<float> *tracks_eta;
    std::vector<float> *tracks_E;
    std::vector<float> *tracks_phi;                                         

  std::vector<float> *proton_px;
  std::vector<float> *proton_py;
  std::vector<float> *proton_pz;
  std::vector<float> *proton_e;

  std::vector<float> *W_Bosons_px;
  std::vector<float> *W_Bosons_py;
  std::vector<float> *W_Bosons_pz;
  std::vector<float> *W_Bosons_e;

  std::vector<float> *Lep_W_Bosons_px;
  std::vector<float> *Lep_W_Bosons_py;
  std::vector<float> *Lep_W_Bosons_pz;
  std::vector<float> *Lep_W_Bosons_e;
  std::vector<float> *Lep_W_Bosons_pid;

  std::vector<float> *Had_W_Bosons_px;
  std::vector<float> *Had_W_Bosons_py;
  std::vector<float> *Had_W_Bosons_pz;
  std::vector<float> *Had_W_Bosons_e;

  float met_et;
  float met_phi;
  std::vector<float> *jet_pt;
  std::vector<float> *jet_eta;
  std::vector<float> *jet_E;
  std::vector<float> *jet_phi;

  std::vector<float> *trkjet_pt;
  std::vector<float> *trkjet_eta;
  std::vector<float> *trkjet_E;
  std::vector<float> *trkjet_phi;



  std::vector<float> *leptons_pt;
  std::vector<float> *leptons_eta;
  std::vector<float> *leptons_E;
  std::vector<float> *leptons_phi;
  std::vector<float> *leptons_charge;
                                                         
  std::vector<float> *neutrinos_pt;
  std::vector<float> *neutrinos_eta;
  std::vector<float> *neutrinos_E;
  std::vector<float> *neutrinos_phi;
  std::vector<float> *neutrinos_charge;
 
  std::vector<float> *weightvec; 

  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(yyWW);

}
